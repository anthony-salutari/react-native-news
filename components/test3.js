import React from 'react';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import Toolbar from './Toolbar';
import NewsSourceContainer from '../containers/NewsSourceContainer';

export default function test3() {
  return(
    <View style={styles.container}>
      <Toolbar />
      <NewsSourceContainer />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e5e5e5',
  }
});
