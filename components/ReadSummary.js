import React, { Component } from 'react';

import {
  Text,
  View
} from 'react-native';

import * as Constants from '../Constants';

const AYLIENTextAPI = require('aylien_textapi');
const textapi = new AYLIENTextAPI({
  application_id: Constants.AYLIEN_APP_ID,
  application_key: Constants.AYLIEN_KEY
});

export default class ReadSummary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      summary: ''
    }
  }

  componentDidMount() {
    textapi.summarize({
      'url': this.props.url
    }, function(error, response) {
      if (error === null) {
        console.log(response);
      }
    });
  }

  render() {
    return(
      <View>
        <Text>Just testing</Text>
      </View>
    );
  }
}
