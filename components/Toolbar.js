import React, { Component } from 'react';

import {
  StyleSheet,
  ToolbarAndroid
} from 'react-native';

import * as Constants from '../Constants';

export default class Toolbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: true
    }
  }

  render() {
    return(
      <ToolbarAndroid
        style={styles.toolbar}
        title='react-native-news'
        titleColor='#ffffff'
        actions={[{title: 'Search', icon: require('../images/magnify.png'), show: 'always'}]} />
    );
  }
}

const styles = StyleSheet.create({
  toolbar: {
    marginTop: 24,
    height: 56,
    elevation: 4,
    backgroundColor: Constants.PRIMARY
  }
});
