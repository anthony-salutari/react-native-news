import React, { Component } from 'react';

import {
  FlatList,
  Text
} from 'react-native';

import * as Constants from '../Constants';
import Article from '../components/Article';

const NewsAPI = require('newsapi');
const newsapi = new NewsAPI(Constants.NEWS_API_KEY);

export default class ArticleContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      articles: [],
      loading: false,
      page: 1,
      error: null,
      refreshing: false,
      offset: 0
    };
  }

  componentDidMount() {
    newsapi.v2.topHeadlines({
      country: 'us'
    }).then(response => {
      this.setState({
        articles: response.articles
      })
    });
  }

  listScrolled = (event) => {
    const currentOffset = event.nativeEvent.contentOffset.y;
    const direction = (currentOffset > 0 && currentOffset > this.state.offset)
      ? 'down' : 'up';
    
  }

  render() {
    return(
      <FlatList
        data={this.state.articles}
        keyExtractor={item => item.url}
        renderItem={({item}) => <Article article={item} />}
        onScroll={this.listScrolled} />
    );
    // this.state.articles.map((article) => {
    //   return(
    //     <FlatList
    //       data={this.state.articles}
    //       renderItem={({item}) => <Article />} />
    //     )
    // })
  }
}
