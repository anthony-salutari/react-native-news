import React, { Component } from 'react';

import {
  FlatList,
  Image,
  Text,
  StyleSheet,
  View,
  Linking
} from 'react-native';

import {
  TextButton
} from 'react-native-material-buttons';

// import {
//   Card,
//   Divider,
// } from 'react-native-elements';

import * as Constants from '../Constants';

export default class Article extends Component {
  constructor(props) {
    super(props);

    this.readMoreButtonPressed = this.readMoreButtonPressed.bind(this);
    this.readSummaryButtonPressed = this.readSummaryButtonPressed.bind(this);
    this.readArticleButtonPressed = this.readSummaryButtonPressed.bind(this);
  }

  readMoreButtonPressed() {
    return false;
  }

  readSummaryButtonPressed() {
    return false;
  }

  readArticleButtonPressed() {
    return false;
  }

  render() {
    return(
      <View style={styles.card}>
        <Image
          style={styles.cardImage}
          source={{uri: this.props.article.urlToImage}} />
        <View style={styles.cardBody}>
          <Text style={styles.titleText}>{this.props.article.title}</Text>
          <Text style={styles.descriptionText}>{this.props.article.description}</Text>
          <View style={styles.buttonView}>
            <TextButton
              title='Read More'
              titleColor={Constants.SECONDARY}
              onPress={() => Linking.openURL(this.props.article.url)}>
            </TextButton>
            <TextButton
              title='Read Out Loud'
              titleColor={Constants.SECONDARY}>
            </TextButton>
          </View>
        </View>
      </View>
      // <Card
      //   style={styles.card}
      //   image={{uri: this.props.article.urlToImage}}
      //   imageProps={{resizeMode: 'cover', height: 250}}>
      //   <View>
      //     <Text style={styles.title}>{this.props.article.title}</Text>
      //     <Text style={styles.body}>{this.props.article.description}</Text>
      //   </View>
      //   <Divider />
      //   <View style={styles.buttonView}>
      //     <Button style={styles.button} title='Read More' onPress={() => Linking.openURL(this.props.article.url)} />
      //     <Button style={styles.button} title='Read Summary' onPress={this.readSummaryButtonPressed} />
      //     <Button style={styles.button} title='Read Article' onPress={this.readArticleButtonPressed} />
      //   </View>
      // </Card>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 2,
    elevation: 1,
    margin: 8,
    backgroundColor: '#ffffff'
  },
  cardImage: {
    height: 200,
    resizeMode: 'cover'
  },
  cardBody: {
    padding: 16
  },
  titleText: {
    fontSize: 24,
    color: '#000000de'
  },
  descriptionText: {
    fontSize: 14,
    marginTop: 8,
    marginBottom: 8,
    color: '#00000099'
  },
  buttonView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
});
