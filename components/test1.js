import React from 'react';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import Toolbar from './Toolbar';
import ArticleContainer from '../containers/ArticleContainer';

export default function test1() {
  return(
    <View style={styles.container}>
      <Toolbar />
      <ArticleContainer />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e5e5e5',
  },
});
