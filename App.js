import React, { Component } from 'react';

import { StyleSheet, Button, ToolbarAndroid, Text, View, StatusBar } from 'react-native';

import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import * as Constants from './Constants';
import Toolbar from './components/Toolbar';
import Article from './components/Article';
import ArticleContainer from './containers/ArticleContainer';

import test1 from './components/test1';
import test2 from './components/test2';
import test3 from './components/test3';

export default class App extends Component {
  render() {
    return (
      //<View style={styles.container}>
        //<StatusBar backgroundColor={Constants.PRIMARY_DARK} />
        //<Toolbar />
          <Navigator />
        //<ArticleContainer />
      //</View>
    );
  }
}

const Navigator = createMaterialBottomTabNavigator({
  'Your Feed': { screen: test2 },
  'Top Headlines': { screen: test1, icon: require('./images/fire.png')  },
  'Sources': { screen: test3, icon: require('./images/newspaper.png')  },
}, {
  initialRouteName: 'Top Headlines',
  // activeTintColor: '#f0edf6',
  // inactiveTintColor: '#3e2465',
  barStyle: { backgroundColor: Constants.PRIMARY },
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e5e5e5',
  },
});
