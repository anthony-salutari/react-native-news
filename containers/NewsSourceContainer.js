import React, { Component } from 'react';

import {
  ScrollView,
  StyleSheet,
  Text,
} from 'react-native';

import * as Constants from '../Constants';
import NewsSource from '../components/NewsSource';

const NewsAPI = require('newsapi');
const newsapi = new NewsAPI(Constants.NEWS_API_KEY);

export default class NewsSourceContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sourceList: [],
    }
  }

  componentDidMount() {
    newsapi.v2.sources().then(response => {
      this.setState({
        sourceList: response.sources
      });
    });
  }

  render() {
    console.log(this.state);
    return(
      <ScrollView style={styles.container}>
        <Text>Hello</Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10
  },
});
