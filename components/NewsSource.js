import React from 'react';

import {
  Text,
  View,
  StyleSheet,
} from 'react-native';

import {
  TextButton
} from 'react-native-material-buttons';

import * as Constants from '../Constants';

export default function NewsSource() {
  return(
    <View style={styles.card}>
      <Text style={styles.name}>{this.props.source.name}</Text>
      <Text style={styles.description}>{this.props.source.description}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 2,
    elevation: 1,
    margin: 8,
    backgroundColor: '#ffffff',
  },
  name: {
    fontSize: 24,
    color: '#000000de',
  },
  description: {
    fontSize: 14,
    marginTop: 8,
    marginBottom: 8,
    color: '#00000099'
  },
});
