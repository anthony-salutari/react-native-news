export const NEWS_API_KEY = 'get your own keys';
export const AYLIEN_KEY = 'get your own keys';
export const AYLIEN_APP_ID = 'get your own keys';

export const PRIMARY = '#2196f3';
export const PRIMARY_DARK = '#6ec6ff';
export const PRIMARY_LIGHT = '#0069c0';

export const SECONDARY = '#ff5722';
export const SECONDARY_DARK = '#c41c00';
export const SECONDARY_LIGHT = '#ff8a50';
