export default createMaterialBottomTabNavigator({
  Album: { screen: test1 },
  Library: { screen: test2 },
  History: { screen: test3 },
}, {
  initialRouteName: 'Album',
  activeTintColor: '#f0edf6',
  inactiveTintColor: '#3e2465',
  barStyle: { backgroundColor: '#694fad' },
});
